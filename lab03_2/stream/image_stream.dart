import 'package:flutter/material.dart';

class LinkImageStream{
  Stream<String> getImages() async*{
    final List<String> vocabularies = [
      "https://scontent.fsgn5-10.fna.fbcdn.net/v/t39.30808-6/275557523_1314008825740795_1734964978314523369_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=FvNyB7xoE9EAX9v1ZJr&_nc_ht=scontent.fsgn5-10.fna&oh=00_AT8lmUgnjDWBio1i5eeFGmAR5WW3MxFBJ4EWQeyC1yNcSQ&oe=6233007D",
      "https://scontent.fsgn5-10.fna.fbcdn.net/v/t39.30808-6/275427286_1312870002521344_5086985753820013584_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=5QMFLrGE5lgAX9-AYNS&_nc_ht=scontent.fsgn5-10.fna&oh=00_AT-q76Jj7VJQFsHTHeTBufYZwopuH_kJ1l8J6Kz-rLNeuQ&oe=6232431D",
      "https://scontent.fsgn5-11.fna.fbcdn.net/v/t39.30808-6/274719345_1306359416505736_1213395982347389662_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=3nkQg00upIsAX_TMgBI&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT_wF71c_Ympfr7kByYzsuj-2e4qjyjoT5NTX5GxlgPKcg&oe=62322685",
      "https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/274680314_1305158549959156_3448964870937112367_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=xBDjo12SBEgAX-zNDAe&tn=_kOdLZOVNg9mquez&_nc_ht=scontent.fsgn5-9.fna&oh=00_AT_guL7J8MHuh55K5CZxZSybyoz-LElzoJtjImN71jqTWQ&oe=62329495",
      "https://scontent.fsgn5-10.fna.fbcdn.net/v/t39.30808-6/274570113_1305045153303829_6531182014461007381_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=YLAuCP4HzzIAX_VW-s5&_nc_ht=scontent.fsgn5-10.fna&oh=00_AT8AhXoZKWaAjxOwjsF22w1mnMuHVQJjIowJmYW1V6QWWg&oe=62334A1A",
      "https://scontent.fsgn5-11.fna.fbcdn.net/v/t39.30808-6/274676135_1305045146637163_8236136455884178223_n.jpg?_nc_cat=110&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=mMR-nuzwovoAX_YQbE_&tn=_kOdLZOVNg9mquez&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT-FcqsXJ3p6xAfndoxE0ji3So2KA4czL7k0aixIylNkMQ&oe=6232B798",
      "https://scontent.fsgn5-12.fna.fbcdn.net/v/t39.30808-6/274614262_1303852503423094_347624642422336143_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=A-_qb8B7DbEAX80ebV4&_nc_ht=scontent.fsgn5-12.fna&oh=00_AT9lbV7UAz92EBIlu48ThCoWL4YzOY_rcOKhDnWZsCMNDQ&oe=623249B7",
      "https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/273182070_1291679204640424_5471041932215402236_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=o6EJvWZyMM8AX8tZZAI&_nc_ht=scontent.fsgn5-9.fna&oh=00_AT-ws_6E6xBoqiP60tuUkiuUIJxjZdYVWQK1ZMql6TWOfw&oe=62322837",
      "https://scontent.fsgn5-14.fna.fbcdn.net/v/t39.30808-6/272912754_1289680901506921_8208446240432106327_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=VnRJ5rQTux8AX_KtkOk&_nc_ht=scontent.fsgn5-14.fna&oh=00_AT86bC6hswnBy8dLNSCVVExXDzHgIQA5kQ3_GbzYKIyJBA&oe=6231932F",
      "https://scontent.fsgn5-10.fna.fbcdn.net/v/t39.30808-6/272714005_1286827788458899_4038713772830476121_n.jpg?_nc_cat=107&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=QUKj4P44dk8AX8qBdoP&_nc_ht=scontent.fsgn5-10.fna&oh=00_AT9qqsJS_iCVPnTGfoqdV76qEEyg2GyF-Hekn9pBa32kcQ&oe=623195FE",
      "https://scontent.fsgn5-13.fna.fbcdn.net/v/t39.30808-6/272687413_1286827771792234_4117719120416164780_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=gneNvi7DPSEAX8xidRZ&_nc_ht=scontent.fsgn5-13.fna&oh=00_AT-jCNgSM91UZMj7aZAM4USamLqbYsieuGvlgHj3nkUepA&oe=6231CEE4",
      "https://scontent.fsgn5-6.fna.fbcdn.net/v/t39.30808-6/272407956_1285520698589608_1255297288398388877_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=nkafcC3inEcAX-ua2vm&_nc_ht=scontent.fsgn5-6.fna&oh=00_AT--9wysIE0XLVxrdY2nOIh5vJen_UviiL4i3HGDixDuRQ&oe=62325F16",
      "https://scontent.fsgn5-13.fna.fbcdn.net/v/t39.30808-6/272315034_1285055871969424_8380500319204508684_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=hmxZPuW1CagAX9lFnQW&_nc_ht=scontent.fsgn5-13.fna&oh=00_AT8ALuJyz3nCDGAQVXo0eDh99aqg4d-DeZtKJsj8x0WHig&oe=6232B600",
      "https://scontent.fsgn5-13.fna.fbcdn.net/v/t39.30808-6/271920595_1281214902353521_8836105514133576901_n.jpg?_nc_cat=106&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=MjGOGL9kHmgAX9wG8Ct&_nc_ht=scontent.fsgn5-13.fna&oh=00_AT-oEsRksZGtLUXWU2dvk2vNGZaDFLQWwa_DvD8CGlZFFg&oe=6232568E",
      "https://scontent.fsgn5-15.fna.fbcdn.net/v/t39.30808-6/271542415_1275922656216079_4027999346906091793_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=thEnbBtJQgQAX_H9lyR&_nc_ht=scontent.fsgn5-15.fna&oh=00_AT9iWfHQIe8rAir28hjLBeTHXjgsBPcTItSdsY_Vv3ZSIw&oe=62321F60",
      "https://scontent.fsgn5-6.fna.fbcdn.net/v/t39.30808-6/271134496_1274035086404836_7887925682736842105_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=6-aXgFE-GaIAX8B0WZ_&_nc_ht=scontent.fsgn5-6.fna&oh=00_AT8khuNKvJVAvp_B2HKVzWRIhVMjMahtRHdKw1EwIi7xlQ&oe=6232153B",
      "https://scontent.fsgn5-14.fna.fbcdn.net/v/t39.30808-6/269618717_1267857737022571_6189550249672907070_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=8bfeb9&_nc_ohc=2FMftiRFGnsAX-n8cQy&_nc_ht=scontent.fsgn5-14.fna&oh=00_AT9QAuPFOwhhRUqrj-zWidwY7SlbiZITLc5nZ8Jfqm7wHw&oe=6231977D"
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) {
      int index = t%vocabularies.length;
      return vocabularies[index];
    });
  }
}