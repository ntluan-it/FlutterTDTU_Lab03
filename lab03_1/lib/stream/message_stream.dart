import 'package:flutter/material.dart';

class MessageStream{
  Stream<String> getMessage() async*{
    final List<String> messageStr = [
      'Perfect', 'Overconfident', 'Furious', 'Infuriating',
      'Rested', 'Swamped', 'Serene',
      'Cautious', 'Reckless', 'Enchanting',
    ];

    yield* Stream.periodic(Duration(seconds: 5), (int t) { // seconds to change words
      int index = t - 1;
      return messageStr[index];
    });
  }
}