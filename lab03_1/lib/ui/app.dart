import 'package:flutter/material.dart';
import '../stream/message_stream.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<StatefulWidget>{
  String message = "Click +  to stream message after 5 seconds";
  MessageStream messageStream = MessageStream();
  late List<String> getmessage = [
    'Perfect', 'Overconfident', 'Furious', 'Infuriating',
    'Rested', 'Swamped', 'Serene',
    'Cautious', 'Reckless', 'Enchanting',
  ];
  int count = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Message',
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Stream Message"),
        ),
         body: ListView.builder(
             itemBuilder: (BuildContext, index){
               return Card(
                 child: ListTile(
                   title: Text(getmessage[index]),
                 ),
               );
             },
           itemCount: count,
           shrinkWrap: true,
           padding: EdgeInsets.all(8),
         ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {

            },
          child: IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              changeString();
            },
          ),
        ),
      ),
    );
  }

  changeString() async{
    messageStream.getMessage().listen((eventColor) {
      setState(() {
        getmessage[count] = eventColor;
        count++;
      });
    });
  }
}
